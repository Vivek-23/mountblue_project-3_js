
function find(elements, cb) {
  if (elements === undefined) return undefined;
  if(cb === undefined) return elements[0];
  let value = undefined;
  if(Array.isArray(elements)){
    for(i=0, length=elements.length; i<length; i++){
      if(cb(elements[i], i, elements)){
          return value = elements[i];
      }
    }
  }else{
    var key = Object.keys(elements);
    for(i=0, length=key.length; i<length; i++){
      if(cb(elements[key[i]], key[i], elements)){
        return value = elements[key[i]];
      }
    }
  }

  return value;
}
module.exports = find;
