const each = require('./each')

function filter(elements, cb) {
  if (elements === undefined) return [];
  if(cb === undefined) return elements;
  let value = [];
  each(elements, function(val, index, list){
    if(cb(val, index, list)) value.push(val)
  })
  return value;
}
module.exports = filter;