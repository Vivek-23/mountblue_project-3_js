const items = require('./arrays_exercise');
const reduce = require('./reduce')
//const underscore = require('underscore')


let result = reduce(items, function(memo, num){ return memo + num; }, 0);
console.log(result);

let result1 = reduce(items, (a,b) => {return a + b}, 2);
console.log(result1);

let result2 = reduce(items, (a,b) => a + b);
console.log(result2);

// let result3 = underscore.reduce({one:1, two:2}, function(memo, num){ return memo + num; }, 0);
// console.log(result3);

let result = reduce({one:1, two:2}, function(memo, num){ return memo + num; }, 0);
console.log(result);