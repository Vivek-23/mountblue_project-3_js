function reduce(elements, cb, startingValue) {
  if (cb == undefined || elements == undefined) return [];
  let startValue = startingValue == undefined ? 1 : 0;
  let reduce = startingValue == undefined ? elements[0] : startingValue;
  // for (let i = startValue; i < elements.length; i++) {
  //   reduce = cb(reduce, elements[i], i);
  // }
  if(Array.isArray(elements)){
    for(i=startValue, length=elements.length; i<length; i++){
      reduce = cb(reduce, elements[i], i);
    }
  }else{
    var key = Object.keys(elements);
    for(i=startValue, length=key.length; i<length; i++){
      reduce = cb(reduce, elements[key[i]], key[i]);
    }
  }
  return reduce;
}

module.exports = reduce;
