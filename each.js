function each(elements, cb) {
  let length, i;
  if(Array.isArray(elements)){
    for(i=0, length=elements.length; i<length; i++){
      cb(elements[i], i, elements);
    }
  }else{
    var key = Object.keys(elements);
    for(i=0, length=key.length; i<length; i++){
      cb(elements[key[i]], key[i], elements);
    }
  }
  return elements;
}
module.exports = each;
