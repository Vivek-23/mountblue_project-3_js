function flatten(elements) {
  if (elements === undefined) return [];
  let result = [];
  function checkArray(array) {
    for (let val of array) {
      if (Array.isArray(val)) {
        checkArray(val);
      } else {
        result.push(val);
      }
    }
  }

  checkArray(elements);
  return result;

  // function checkflatten(elements, depth, strict, output){
  //   output = output || [];
  //   if(!depth && depth != 0){
  //     depth = Infinity;
  //   }else if(depth <= 0){
  //     return output.concat(elements);
  //   }

  //   let index = output.length;
  //   for(let i=0, length = elements.length; i<length; i++){
  //     let value = elements[i];
  //     if(Array.isArray(value)){
  //       if(depth > 1){
  //         checkflatten(value, depth-1, strict, output);
  //         index = output.length;
  //       }else{
  //         let j=0, len = value.length;
  //         while(j < len) output[index++] = value[j++];
  //       }
  //     }else if(!strict){
  //       output[index++] = value;
  //     }
  //   }
  //   return output;
  // }
  
  // return checkflatten(elements, depth, false)
}

module.exports = flatten;
