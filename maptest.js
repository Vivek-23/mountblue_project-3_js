const items = require('./arrays_exercise');
const map = require('./map');
//const underscore = require('underscore')


let result = map(items, (item) => Math.pow(item, 2));
console.log(result);

let result1 = map({one:1, two: 2}, (item) => Math.pow(item, 2));
console.log(result1)

// let result2 = underscore.map({one:1, two: 2}, (item) => Math.pow(item, 2));
// console.log(result2)