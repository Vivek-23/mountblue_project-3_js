const each = require('./each')

function map(elements, cb) {
    if(elements == undefined) return [];
    if(cb === undefined) return elements
    let array = []
    each(elements, (val) => {
        array.push(cb(val));
    })
    return array;    

}
module.exports = map;