const items = require('./arrays_exercise')
const filter = require('./filter');
//const underscore = require('underscore')


let result = filter(items, (item) => {return item % 2 == 0});
console.log(result);

let result1 = filter({one:1, two:2, three:3, four:4}, (item) => {return item % 2 == 0});
console.log(result1);

let result2 = filter(items)
console.log(result2);

// let result3 = underscore.filter(items, (item)=> {return item%2 == 0})
// console.log(result3)

// let result4 = underscore.filter({one:1, two:2, three:3, four:4}, (item) => {return item % 2 == 0});
// console.log(result4);