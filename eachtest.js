const items = require('./arrays_exercise');
const each = require('./each');
//const underscore  = require('underscore')


let result = each(items, (item) => console.log(item))
console.log(result);
// let result2 = underscore.each(items, (item) => console.log(item));
// console.log(result2)

let result3 = each({one: 1, two: 2, three:3}, (item) => console.log(item))
console.log(result3);
// let result4 = underscore.each({one: 1, two: 2, three:3}, (item) => console.log(item));
// console.log(result4)