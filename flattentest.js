const flatten = require("./flatten");
// const underscore = require('underscore')

const nestedArray = [1, [2], [[3]], [[[4]]]];


let result = flatten(nestedArray);
console.log(result);

let result1 = flatten([[[[11]]],5, [2], [[6]], [[[3]]]]);
console.log(result1);

let result4 = flatten()
console.log(result4)

// let result2 = underscore.flatten();
// console.log(result2);

// let result3 = underscore.flatten([[[[11]]],5, [2], [[6]], [[[3]]]]);
// console.log(result3);

